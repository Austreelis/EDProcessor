use crate::input_field::InputField;
use ed_processor::modules::cores::fsd::{ParseFSDError, FSD};
use ed_processor::{LightYears, ParseUnitError, Tons};
use yew::macros::html;
use yew::{Component, ComponentLink, Html};

pub(crate) type FSDValue = Option<Result<FSD, ParseFSDError>>;
pub(crate) type LightYearsValue = Option<Result<LightYears, ParseUnitError>>;
pub(crate) type TonsValue = Option<Result<Tons, ParseUnitError>>;

pub(crate) struct FuelLevelProcessor {
    link: ComponentLink<Self>,
    fsd: FSDValue,
    max_range: LightYearsValue,
    current_range: LightYearsValue,
    cargo: TonsValue,
}

pub(crate) enum ProcessorMsg {
    OnFsdInput(usize, FSDValue),
    OnLightYearsInput(usize, LightYearsValue),
    OnTonsInput(usize, TonsValue),
}

impl Component for FuelLevelProcessor {
    type Message = ProcessorMsg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        FuelLevelProcessor {
            link,
            fsd: None,
            max_range: None,
            current_range: None,
            cargo: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            ProcessorMsg::OnFsdInput(0, fsd) => {
                self.fsd = fsd;
            }
            ProcessorMsg::OnLightYearsInput(1, max_range) => {
                self.max_range = max_range;
            }
            ProcessorMsg::OnLightYearsInput(2, current_range) => {
                self.current_range = current_range;
            }
            ProcessorMsg::OnTonsInput(3, cargo) => {
                self.cargo = cargo;
            }
            _ => unreachable!(),
        };
        true
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let fsd_callback = self.link.callback(|fsd| ProcessorMsg::OnFsdInput(0, fsd));
        let max_range_callback = self
            .link
            .callback(|max_range| ProcessorMsg::OnLightYearsInput(1, max_range));
        let current_range_callback = self
            .link
            .callback(|current_range| ProcessorMsg::OnLightYearsInput(2, current_range));
        let cargo_callback = self
            .link
            .callback(|cargo| ProcessorMsg::OnTonsInput(3, cargo));
        let value = if let (Some(Ok(fsd)), Some(Ok(max_range)), Some(Ok(current_range)), cargo) =
            (self.fsd, self.max_range, self.current_range, self.cargo)
        {
            let cargo = cargo.unwrap_or(Ok(0.0.into())).unwrap_or(0.0.into());
            let residual_mass = fsd.residual_mass(max_range, current_range);

            if *residual_mass < *cargo {
                format!("unladen mass of {} is greater than current mass without cargo of {} did you mixed up the numbers ?",
                                     Tons::from(*fsd.range2unladen_mass(current_range) - *cargo),
                                     fsd.range2current_mass(max_range)
                )
            } else {
                format!("{}", Tons::from(*residual_mass - *cargo))
            }
        } else {
            "".to_string()
        };
        html! {
            <table>
                <thead><h3>{"Fuel Level"}</h3></thead>
                <tbody>
                    <InputField<FSD, ParseFSDError>
                        name="FSD class & rating"
                        callback=fsd_callback
                    />
                    <InputField<LightYears, ParseUnitError>
                        name="Maximum range [Ly]"
                        callback=max_range_callback
                    />
                    <InputField<LightYears, ParseUnitError>
                        name="Current range [Ly]"
                        callback=current_range_callback
                    />
                    <InputField<Tons, ParseUnitError>
                        name="Cargo weight [T]"
                        callback=cargo_callback
                    />
                    <tr>
                        <td>{"Fuel left in tank"}</td>
                        <td>{value}</td>
                    </tr>
                </tbody>
            </table>
        }
    }
}
