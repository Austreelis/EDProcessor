use std::fmt;
use std::str::FromStr;
use yew::macros::html;
use yew::{Callback, Component, ComponentLink, Html, InputData, Properties};

pub(crate) struct InputField<T, E>
where
    T: 'static + FromStr<Err = E> + fmt::Display + PartialEq + Clone,
    E: 'static + fmt::Display + PartialEq + Clone,
{
    link: ComponentLink<Self>,
    props: InputFieldProps<T, E>,
    value: Option<Result<T, E>>,
}

#[derive(Properties, PartialEq, Clone)]
pub(crate) struct InputFieldProps<T, E>
where
    T: PartialEq + Clone,
    E: PartialEq + Clone,
{
    pub(crate) callback: Callback<Option<Result<T, E>>>,
    pub(crate) name: String,
    #[prop_or_default]
    pub(crate) required: bool,
}

impl<T, E> Component for InputField<T, E>
where
    T: 'static + FromStr<Err = E> + fmt::Display + PartialEq + Clone,
    E: 'static + fmt::Display + PartialEq + Clone,
{
    type Message = Option<Result<T, E>>;
    type Properties = InputFieldProps<T, E>;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        InputField {
            link,
            props,
            value: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        if self.value != msg {
            self.value = msg.clone();
            self.props.callback.emit(msg);
            true
        } else {
            false
        }
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html! {
            <tr>
                <td>{self.props.name.as_str()}</td>
                <td>
                    <input oninput=self.link.callback(|input: InputData| {
                        let value = input.value;
                        if value.trim().is_empty() {
                            None
                        } else {
                            Some(T::from_str(value.as_str()))
                        }
                    })/>
                </td>
                {
                    match &self.value {
                        None => {
                            if self.props.required {
                                html! {<td>{("field required".to_string())}</td>}
                            } else {
                                html! {}
                            }
                        },
                        Some(res) => html! {
                            <td>{match res {
                                Ok(v) => v.to_string(),
                                Err(e) => e.to_string(),
                                }}
                            </td>
                        },
                    }
                }
            </tr>
        }
    }
}
