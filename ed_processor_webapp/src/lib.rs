#![recursion_limit = "1024"]
mod input_field;
mod processor;

use crate::processor::FuelLevelProcessor;
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::JsValue;
use web_sys::console;
use yew::App;

#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen(start)]
pub fn main_js() -> Result<(), JsValue> {
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();

    console::log_1(&JsValue::from_str(
        format!("EDProcessor Web UI v{}", env!("CARGO_PKG_VERSION")).as_str(),
    ));

    App::<FuelLevelProcessor>::new().mount_to_body();

    Ok(())
}
