use clap::Clap;
use ed_processor::modules::cores::fsd::FSD;
use ed_processor::{LightYears, Tons};

enum ExitCodes {
    Ok,
    #[allow(dead_code)]
    ArgParseError, // Clap exits with 1 if argument parsing fails
    InvalidInputData,
}

/// Swiss-knife tool to compute things related to Elite: Dangerous, a game by Frontier Developments
#[derive(Clap)]
#[clap(
    name = "EDProcessor",
    version = "0.0.1",
    author = "Copyright © 2020 Morgane Austreelis <morgane.austreelis@gmail.com>
This program is licensed under the General Public License v3.0"
)]
struct Opts {
    /// Suppress all unnecessary outputs
    #[clap(short, long)]
    quiet: Option<bool>,
    #[clap(subcommand)]
    sub: Commands,
}

#[derive(Clap)]
enum Commands {
    #[clap(
        version = "0.1",
        author = "Copyright © 2020 Morgane Austreelis <morgane.austreelis@gmail.com>
This program is licensed under the General Public License v3.0"
    )]
    FuelUsage(FuelUsageOpts),
    #[clap(
        version = "0.1",
        author = "Copyright © 2020 Morgane Austreelis <morgane.austreelis@gmail.com>
This program is licensed under the General Public License v3.0"
    )]
    JumpRange(JumpRangeOpts),
    #[clap(
        version = "0.1",
        author = "Copyright © 2020 Morgane Austreelis <morgane.austreelis@gmail.com>
This program is licensed under the General Public License v3.0"
    )]
    FuelLevel(FuelLevelOpts),
}

#[derive(Clap)]
/// Compute jump fuel usage
struct FuelUsageOpts {
    /// The equipped FSD's class and rating, eg '5A'
    fsd: FSD,
    /// The total mass of the ship before jumping, eg '42.6'
    ship_mass: Tons,
    /// The distance of the jump, eg '84.2'
    range: LightYears,
}

impl FuelUsageOpts {
    fn exec(&self, quiet: bool) -> ExitCodes {
        match self.fsd.jump_fuel_consumption(self.range, self.ship_mass) {
            Ok(fuel) => {
                println!("{}", fuel);
            }
            Err(fuel) => {
                if !quiet {
                    eprintln!(
                        "{} tons exceeds max fuel consumption possible of {} tons",
                        fuel,
                        self.fsd.max_fuel_consumption()
                    )
                }
                return ExitCodes::InvalidInputData;
            }
        }

        ExitCodes::Ok
    }
}

/// Compute jump range
#[derive(Clap)]
struct JumpRangeOpts {
    /// The equipped FSD's class and rating, eg '5A'
    fsd: FSD,
    /// The total mass of the ship before jumping, eg '42.6'
    ship_mass: Tons,
    /// The fuel used for jumping
    fuel_used: Option<Tons>,
}

impl JumpRangeOpts {
    fn exec(&self, quiet: bool) -> ExitCodes {
        match self.fsd.jump_distance(
            self.fuel_used.unwrap_or(self.fsd.max_fuel_consumption()),
            self.ship_mass,
        ) {
            Ok(distance) => println!("{}", distance),
            Err(distance) => {
                if !quiet {
                    eprintln!(
                        "{} light years exceeds max range possible of {} light years",
                        distance,
                        self.fsd
                            .jump_distance(self.fsd.max_fuel_consumption(), self.ship_mass)
                            .unwrap(),
                    )
                }
                return ExitCodes::InvalidInputData;
            }
        }

        ExitCodes::Ok
    }
}

/// Compute fuel tank level
#[derive(Clap)]
struct FuelLevelOpts {
    /// The equipped FSD's class and rating, eg '5A'
    fsd: FSD,
    /// The maximum range displayed in the ship's internals panel
    max_range: LightYears,
    /// The current range displayed in the ship's internals panel
    current_range: LightYears,
    /// The extra payload mass in the ship that isn't fuel. If not specified, the result is the
    /// whole payload mass, including cargo. Otherwise, extra payload mass is deducted from the
    /// result and the actual mass of fuel left is returned.
    cargo_mass: Option<Tons>,
}

impl FuelLevelOpts {
    fn exec(&self, quiet: bool) -> ExitCodes {
        let residual_mass = *self.fsd.residual_mass(self.max_range, self.current_range);

        if residual_mass < *self.cargo_mass.unwrap_or(0.0.into()) {
            if !quiet {
                eprintln!(
                    "unladen mass of {} is greater than current mass without cargo of {}
            did you mixed up the numbers ?",
                    Tons::from(
                        *self.fsd.range2unladen_mass(self.current_range)
                            - *self.cargo_mass.unwrap_or(0.0.into())
                    ),
                    self.fsd.range2current_mass(self.max_range)
                )
            }
            return ExitCodes::InvalidInputData;
        } else {
            println!("{}", residual_mass);
        }

        ExitCodes::Ok
    }
}

fn main() {
    let opts: Opts = Opts::parse();
    let quiet = opts.quiet.unwrap_or(false);
    std::process::exit(match &opts.sub {
        Commands::FuelUsage(cmd) => cmd.exec(quiet),
        Commands::JumpRange(cmd) => cmd.exec(quiet),
        Commands::FuelLevel(cmd) => cmd.exec(quiet),
    } as i32)
}
