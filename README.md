# EDProcessor

### About 
Swiss-knife tooling for [Elite: Dangerous](https://www.elitedangerous.com/),
a game by [Frontier Developments](https://www.frontier.co.uk/).

The content of this repository is licensed under the terms of the
[GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

The authors are **not** affiliated with Frontier Developments,
Elite: Dangerous is a trademark of Frontier Development.

EDProcessor © 2020 - 2021 Morgane Austreelis

Elite: Dangerous © 2012, 2013 Frontier Developments plc. All rights reserved.

## How to use

There is currently no release available for EDProcessor, but if you wish to
still use it, the following instructions should hopefully be of use.

Before that, quick reminder that this project is in its very early stage of
development and is entirely done during free time. In that regard, it may never
reach a "completed" or even "stable" state, nor be adequately maintained.

### Installing rust

To build anything in this project, you will need to setup `rustup`, a 
command-line tool for managing rust versions and associated tools.
> If you don't want to install `rustup`,
[read here](https://forge.rust-lang.org/infra/other-installation-methods.html)

For Windows, get `rustup` [here](https://www.rust-lang.org/tools/install),
you will also need the
[Build Tools for Visual Studio 2019](https://visualstudio.microsoft.com/visual-cpp-build-tools/)

For Linux or MacOS, run:
```bash
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```

Once `rustup`  is installed, run 
```
rustup default stable
```
then 
```
rustup update
```
to ensure you have the latest stable rust release.

Once you're done, running `rustc --version` should give you something like
`rustc x.y.z (abcabcabc yyyy-mm-dd)`. Congrats ! You have installed rust !

By installing rust via `rustup`, you also have installed `cargo`, the rust
build tool.

### EDProcessor cli

This project includes the crate `ed_processor_cli`, under the folder of the 
same name, which is a command-line utility for the `ed_processor` library.

to build it, go under the crate's directory and run `cargo build`. 
This will compile a development version of the binary, and place the 
executable under `target/debug`.

You can run the executable directly from cargo, using `cargo run`.
To pass arguments to the tool, append them after a double dash, like so:
`cargo run -- arg1 arg2 ... argn`

### EDProcessor web app

This project includes the crate `ed_processor_webapp`, under the folder of the
same name, which is a web interface for the `ed_processor` library.

To build it, you will need to [install npm](https://www.npmjs.com/get-npm)

Once you have checked your install, go under the crate's directory and run
`npm start`. This should build the project using Cargo and start a web server
serving the compiled files. Your browser should open as soon as you type it.
The first time you will build the project, it will likely take a while, and
your browser may timeout before building is finished. Simply reload the page
when it is done if that happens.

This project uses the rust-wasm bindings and webpack, meaning you can edit the
rust code and see your modifications in (almost) live.

