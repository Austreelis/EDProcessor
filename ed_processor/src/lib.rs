use std::fmt;
use std::ops::Deref;
use std::str::FromStr;

#[deny(missing_docs, unused_imports, missing_debug_implementations)]
#[macro_use]
pub mod modules;

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum ParseUnitError {
    ParseFloatError,
    IsNegative,
    IsNaN,
}

impl fmt::Display for ParseUnitError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ParseUnitError::ParseFloatError => write!(f, "invalid float literal"),
            ParseUnitError::IsNegative => write!(f, "cannot be negative"),
            ParseUnitError::IsNaN => write!(f, "cannot be NaN"),
        }
    }
}

/// The type used for mass
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Tons(f64);

impl Deref for Tons {
    type Target = f64;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<f64> for Tons {
    fn from(v: f64) -> Self {
        Tons(v)
    }
}

impl From<u32> for Tons {
    fn from(v: u32) -> Self {
        Self::from(v as f64)
    }
}

impl fmt::Display for Tons {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:.3} t", self.0)
    }
}

impl FromStr for Tons {
    type Err = ParseUnitError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Tons(parse_unit(s)?))
    }
}

/// The type used for inter-system distance
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct LightYears(f64);

impl Deref for LightYears {
    type Target = f64;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<f64> for LightYears {
    fn from(v: f64) -> Self {
        LightYears(v)
    }
}

impl From<u32> for LightYears {
    fn from(v: u32) -> Self {
        Self::from(v as f64)
    }
}

impl fmt::Display for LightYears {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:.3} Ly", self.0)
    }
}

impl FromStr for LightYears {
    type Err = ParseUnitError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(LightYears(parse_unit(s)?))
    }
}

fn parse_unit<U>(s: &str) -> Result<U, ParseUnitError>
where
    U: From<f64>,
{
    Ok(match s.parse::<f64>() {
        Ok(v) => {
            if v == f64::NAN {
                return Err(ParseUnitError::IsNaN);
            } else if v < 0.0 {
                return Err(ParseUnitError::IsNegative);
            } else {
                v.into()
            }
        }
        Err(_) => {
            return Err(ParseUnitError::ParseFloatError);
        }
    })
}
