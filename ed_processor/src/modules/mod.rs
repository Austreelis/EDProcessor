//! A module for all ships' modules

use crate::modules::cores::CoreModule;
use crate::modules::hardpoints::HardpointModule;
use crate::modules::military::MilitaryModule;
use crate::modules::optionals::OptionalModule;
use crate::modules::utilities::UtilityModule;
use std::convert::TryFrom;
use std::fmt;
use std::fmt::{Debug, Display, Formatter};
use std::string::ToString;

macro_rules! class {
    (0) => {
        Class::Zero
    };
    (1) => {
        Class::One
    };
    (2) => {
        Class::Two
    };
    (3) => {
        Class::Three
    };
    (4) => {
        Class::Four
    };
    (5) => {
        Class::Five
    };
    (6) => {
        Class::Six
    };
    (7) => {
        Class::Seven
    };
    (8) => {
        Class::Eight
    };
}

#[allow(unused_macros)]
macro_rules! rating {
    (A) => {
        Rating::A
    };
    (B) => {
        Rating::B
    };
    (C) => {
        Rating::C
    };
    (D) => {
        Rating::D
    };
    (E) => {
        Rating::E
    };
    (F) => {
        Rating::F
    };
    (G) => {
        Rating::G
    };
    (H) => {
        Rating::H
    };
    (I) => {
        Rating::I
    };
}

#[allow(unused_macros)]
macro_rules! optional_slot {
    ($c:tt) => {
        OptionalSlot {
            class: class!($c),
            module: None,
        }
    };
    ($c1:tt, $c2:tt) => {
        [optional_slot!($c1), optional_slot!($c2)]
    };
    ($c1:tt, $c2:tt, $c3:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
            optional_slot!($c4),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
            optional_slot!($c4),
            optional_slot!($c5),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
            optional_slot!($c4),
            optional_slot!($c5),
            optional_slot!($c6),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt, $c7:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
            optional_slot!($c4),
            optional_slot!($c5),
            optional_slot!($c6),
            optional_slot!($c7),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt, $c7:tt, $c8:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
            optional_slot!($c4),
            optional_slot!($c5),
            optional_slot!($c6),
            optional_slot!($c7),
            optional_slot!($c8),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt, $c7:tt, $c8:tt, $c9:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
            optional_slot!($c4),
            optional_slot!($c5),
            optional_slot!($c6),
            optional_slot!($c7),
            optional_slot!($c8),
            optional_slot!($c9),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt, $c7:tt, $c8:tt, $c9:tt, $c10:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
            optional_slot!($c4),
            optional_slot!($c5),
            optional_slot!($c6),
            optional_slot!($c7),
            optional_slot!($c8),
            optional_slot!($c9),
            optional_slot!($c10),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt, $c7:tt, $c8:tt, $c9:tt, $c10:tt, $c11:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
            optional_slot!($c4),
            optional_slot!($c5),
            optional_slot!($c6),
            optional_slot!($c7),
            optional_slot!($c8),
            optional_slot!($c9),
            optional_slot!($c10),
            optional_slot!($c11),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt, $c7:tt, $c8:tt, $c9:tt, $c10:tt, $c11:tt, $c12:tt) => {
        [
            optional_slot!($c1),
            optional_slot!($c2),
            optional_slot!($c3),
            optional_slot!($c4),
            optional_slot!($c5),
            optional_slot!($c6),
            optional_slot!($c7),
            optional_slot!($c8),
            optional_slot!($c9),
            optional_slot!($c10),
            optional_slot!($c11),
            optional_slot!($c12),
        ]
    };
}

#[allow(unused_macros)]
macro_rules! military_slot {
    ($c:tt) => {
        MilitarySlot {
            class: class!($c),
            module: None,
        }
    };
    ($c1:tt, $c2:tt) => {
        [military_slot!($c1), military_slot!($c2)]
    };
    ($c1:tt, $c2:tt, $c3:tt) => {
        [
            military_slot!($c1),
            military_slot!($c2),
            military_slot!($c3),
        ]
    };
}

#[allow(unused_macros)]
macro_rules! hardpoint_slot {
    ($c:tt) => {
        HardpointSlot {
            class: class!($c),
            module: None,
        }
    };
    ($c1:tt, $c2:tt) => {
        [hardpoint_slot!($c1), hardpoint_slot!($c2)]
    };
    ($c1:tt, $c2:tt, $c3:tt) => {
        [
            hardpoint_slot!($c1),
            hardpoint_slot!($c2),
            hardpoint_slot!($c3),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt) => {
        [
            hardpoint_slot!($c1),
            hardpoint_slot!($c2),
            hardpoint_slot!($c3),
            hardpoint_slot!($c4),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt) => {
        [
            hardpoint_slot!($c1),
            hardpoint_slot!($c2),
            hardpoint_slot!($c3),
            hardpoint_slot!($c4),
            hardpoint_slot!($c5),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt) => {
        [
            hardpoint_slot!($c1),
            hardpoint_slot!($c2),
            hardpoint_slot!($c3),
            hardpoint_slot!($c4),
            hardpoint_slot!($c5),
            hardpoint_slot!($c6),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt, $c7:tt) => {
        [
            hardpoint_slot!($c1),
            hardpoint_slot!($c2),
            hardpoint_slot!($c3),
            hardpoint_slot!($c4),
            hardpoint_slot!($c5),
            hardpoint_slot!($c6),
            hardpoint_slot!($c7),
        ]
    };
    ($c1:tt, $c2:tt, $c3:tt, $c4:tt, $c5:tt, $c6:tt, $c7:tt, $c8:tt) => {
        [
            hardpoint_slot!($c1),
            hardpoint_slot!($c2),
            hardpoint_slot!($c3),
            hardpoint_slot!($c4),
            hardpoint_slot!($c5),
            hardpoint_slot!($c6),
            hardpoint_slot!($c7),
            hardpoint_slot!($c8),
        ]
    };
}

#[allow(unused_macros)]
macro_rules! utility_slot {
    () => {
        UtilitySlot {
            class: Class::Zero,
            module: None,
        }
    };
    (1) => {
        [utility_slot()]
    };
    (2) => {
        [utility_slot!(), utility_slot!()]
    };
    (3) => {
        [utility_slot!(), utility_slot!(), utility_slot!()]
    };
    (4) => {
        [
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
        ]
    };
    (5) => {
        [
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
        ]
    };
    (6) => {
        [
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
        ]
    };
    (7) => {
        [
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
        ]
    };
    (8) => {
        [
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
            utility_slot!(),
        ]
    };
}

pub mod cores;
pub mod hardpoints;
pub mod military;
pub mod optionals;
pub mod utilities;

/// An enum of all possible modules' ratings
#[derive(Eq, PartialEq, Copy, Clone)]
pub enum Rating {
    #[allow(missing_docs)]
    A,
    #[allow(missing_docs)]
    B,
    #[allow(missing_docs)]
    C,
    #[allow(missing_docs)]
    D,
    #[allow(missing_docs)]
    E,
    #[allow(missing_docs)]
    F,
    #[allow(missing_docs)]
    G,
    #[allow(missing_docs)]
    H,
    #[allow(missing_docs)]
    I,
}
#[deny(missing_docs)]

impl Display for Rating {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Rating::A => "A",
                Rating::B => "B",
                Rating::C => "C",
                Rating::D => "D",
                Rating::E => "E",
                Rating::F => "F",
                Rating::G => "G",
                Rating::H => "H",
                Rating::I => "I",
            }
        )
    }
}

impl TryFrom<&str> for Rating {
    type Error = ();

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        match s.to_lowercase().as_ref() {
            "a" => Ok(Rating::A),
            "b" => Ok(Rating::B),
            "c" => Ok(Rating::C),
            "d" => Ok(Rating::D),
            "e" => Ok(Rating::E),
            "f" => Ok(Rating::F),
            "g" => Ok(Rating::G),
            "h" => Ok(Rating::H),
            "i" => Ok(Rating::I),
            _ => Err(()),
        }
    }
}

impl Debug for Rating {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Rating::{}", self.to_string())
    }
}

/// An enum of all possible modules' classes
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum Class {
    /// The size 0 class, only used for utilities
    Zero,
    /// The size 1 class, used for core, optional, military and hardpoint modules
    One,
    /// The size 2 class, used for core, optional, military and hardpoint modules
    Two,
    /// The size 3 class, used for core, optional, military and hardpoint modules
    Three,
    /// The size 4 class, used for core, optional, military and hardpoint modules
    Four,
    /// The size 5 class, used for core, optional and military modules
    Five,
    /// The size 6 class, used for core and optional modules
    Six,
    /// The size 7 class, used for core and optional modules
    Seven,
    /// The size 8 class, used for core and optional modules
    Eight,
}

impl TryFrom<u8> for Class {
    type Error = ();

    fn try_from(u: u8) -> Result<Self, Self::Error> {
        match u {
            0 => Ok(Class::Zero),
            1 => Ok(Class::One),
            2 => Ok(Class::Two),
            3 => Ok(Class::Three),
            4 => Ok(Class::Four),
            5 => Ok(Class::Five),
            6 => Ok(Class::Six),
            7 => Ok(Class::Seven),
            8 => Ok(Class::Eight),
            _ => Err(()),
        }
    }
}

impl fmt::Display for Class {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Class::Zero => "0",
                Class::One => "1",
                Class::Two => "2",
                Class::Three => "3",
                Class::Four => "4",
                Class::Five => "5",
                Class::Six => "6",
                Class::Seven => "7",
                Class::Eight => "9",
            }
        )
    }
}

impl fmt::Debug for Class {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", *self as u8)
    }
}

/// An enum for all kinds of modules, to allow passing one type for any module.
/// Each value of this enum represent a slot type, the same way human and guardian power plants
/// (for instance) are both abstracted as a kind of generic power plant (that is, a module that
/// provides power) in game.
pub enum Modules {
    /// A module to be mounted in one of the ship's core internal slots
    Core(CoreModule),
    /// A module to be mounted in one of the ship's optional slots
    Optional(OptionalModule),
    /// A module to be mounted in one of the ship's military slots
    Military(MilitaryModule),
    /// A module to be mounted in one the ship's hardpoint slots
    Hardpoint(HardpointModule),
    /// A module to be mounted in one the ship's utility slots
    Utility(UtilityModule),
}

impl Modules {
    /// Get the class of the underlying module
    pub fn class(&self) -> Class {
        match self {
            Modules::Core(core) => core.class(),
            Modules::Optional(optional) => optional.class(),
            Modules::Military(military) => military.class(),
            Modules::Hardpoint(hardpoint) => hardpoint.class(),
            Modules::Utility(utilities) => utilities.class(),
        }
    }
}

impl fmt::Debug for Modules {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Md(")?;
        match self {
            Modules::Core(core) => write!(f, "{:?}", core),
            Modules::Optional(optional) => write!(f, "{:?}", optional),
            Modules::Military(military) => write!(f, "{:?}", military),
            Modules::Hardpoint(hardpoint) => write!(f, "{:?}", hardpoint),
            Modules::Utility(utility) => write!(f, "{:?}", utility),
        }?;
        write!(f, ")")
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use crate::modules::Rating;
    use crate::modules::Rating::*;

    #[test]
    fn string_conversion_is_reflexive() {
        for rating in [A, B, C, D, E, F, G, H, I].iter() {
            let r = Rating::try_from(rating.to_string().as_str());
            assert_eq!(Ok(*rating), r);
        }
    }
}
