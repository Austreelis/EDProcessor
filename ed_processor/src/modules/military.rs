//! Military modules

use crate::modules::Class;
use std::fmt;

/// An enum for all military modules
pub enum MilitaryModule {
    /// A hull reinforcement module
    HullReinforcement(HullReinforcement),
}

impl MilitaryModule {
    /// Get the class of the underlying module
    pub fn class(&self) -> Class {
        match self {
            MilitaryModule::HullReinforcement(hull_reinforcement) => hull_reinforcement.class,
        }
    }
}

impl fmt::Debug for MilitaryModule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Ml(")?;
        match self {
            MilitaryModule::HullReinforcement(hull_reinforcement) => {
                write!(f, "{:?}", hull_reinforcement)
            }
        }?;
        write!(f, ")")
    }
}

/// A slot that can fit a Military module
#[derive(Debug)]
pub struct MilitarySlot {
    /// The class of the slot, only military modules of class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<MilitaryModule>,
}

/// The hull reinforcement module
#[derive(Debug)]
pub struct HullReinforcement {
    /// The class of the module
    pub class: Class,
}
