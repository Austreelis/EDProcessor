//! Optional modules

use crate::modules::Class;
use std::fmt;

/// An enum for all optional modules
pub enum OptionalModule {
    /// A planetary vehicle hangar
    SRVHangar(SRVHangar),
}

impl OptionalModule {
    /// Get the class of the underlying module
    pub fn class(&self) -> Class {
        match self {
            OptionalModule::SRVHangar(srv_hangar) => srv_hangar.class,
        }
    }
}

impl fmt::Debug for OptionalModule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "O(")?;
        match self {
            OptionalModule::SRVHangar(srv_hangar) => {
                write!(f, "{:?}", srv_hangar)
            }
        }?;
        write!(f, ")")
    }
}

/// A slot that can fit an Optional module
#[derive(Debug)]
pub struct OptionalSlot {
    /// The class of the slot, only optional modules of class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<OptionalModule>,
}

/// The SRV hangar module
#[derive(Debug)]
pub struct SRVHangar {
    /// The class of the module
    pub class: Class,
}
