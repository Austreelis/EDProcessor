//! FSD module

use crate::modules::{Class, Rating};
use crate::{LightYears, Tons};
use std::convert::{TryFrom, TryInto};
use std::fmt;
use std::str::FromStr;

/// The Frame Shift Drive module
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct FSD {
    /// The class of the module
    pub class: Class,
    /// The rating of the module
    pub rating: Rating,
}

impl FSD {
    /// Create a new FSD module of given class and rating
    pub fn new(class: Class, rating: Rating) -> Option<Self> {
        if !match rating {
            Rating::A => true,
            Rating::B => true,
            Rating::C => true,
            Rating::D => true,
            Rating::E => true,
            _ => false,
        } || !match class {
            Class::Two => true,
            Class::Three => true,
            Class::Four => true,
            Class::Five => true,
            Class::Six => true,
            Class::Seven => true,
            _ => false,
        } {
            return None;
        }
        Some(FSD { class, rating })
    }

    /// Get the class of this module
    #[inline(always)]
    pub fn class(&self) -> Class {
        self.class
    }

    /// Get the optimal mass of this FSD in Tons
    #[inline(always)]
    pub fn optimal_mass(&self) -> Tons {
        #[allow(unused_parens)]
        (match (self.class as u8, self.rating) {
            (2, Rating::E) => 48,
            (2, Rating::D) => 54,
            (2, Rating::C) => 60,
            (2, Rating::B) => 75,
            (2, Rating::A) => 90,
            (3, Rating::E) => 80,
            (3, Rating::D) => 90,
            (3, Rating::C) => 100,
            (3, Rating::B) => 125,
            (3, Rating::A) => 150,
            (4, Rating::E) => 280,
            (4, Rating::D) => 315,
            (4, Rating::C) => 350,
            (4, Rating::B) => 438,
            (4, Rating::A) => 525,
            (5, Rating::E) => 560,
            (5, Rating::D) => 630,
            (5, Rating::C) => 700,
            (5, Rating::B) => 875,
            (5, Rating::A) => 1050,
            (6, Rating::E) => 960,
            (6, Rating::D) => 1080,
            (6, Rating::C) => 1200,
            (6, Rating::B) => 1500,
            (6, Rating::A) => 1800,
            (7, Rating::E) => 1440,
            (7, Rating::D) => 1620,
            (7, Rating::C) => 1800,
            (7, Rating::B) => 2250,
            (7, Rating::A) => 2700,
            _ => unreachable!(),
        })
        .into()
    }

    /// Get the maximum amount of fuel in tons this FSD can consume per jump
    #[inline(always)]
    pub fn max_fuel_consumption(&self) -> Tons {
        (match (self.class as u8, self.rating) {
            (2, Rating::E) => 0.6,
            (2, Rating::D) => 0.6,
            (2, Rating::C) => 0.6,
            (2, Rating::B) => 0.8,
            (2, Rating::A) => 0.9,
            (3, Rating::E) => 1.2,
            (3, Rating::D) => 1.2,
            (3, Rating::C) => 1.2,
            (3, Rating::B) => 1.5,
            (3, Rating::A) => 1.8,
            (4, Rating::E) => 2.0,
            (4, Rating::D) => 2.0,
            (4, Rating::C) => 2.0,
            (4, Rating::B) => 2.5,
            (4, Rating::A) => 3.0,
            (5, Rating::E) => 3.3,
            (5, Rating::D) => 3.3,
            (5, Rating::C) => 3.3,
            (5, Rating::B) => 4.1,
            (5, Rating::A) => 5.0,
            (6, Rating::E) => 5.3,
            (6, Rating::D) => 5.3,
            (6, Rating::C) => 5.3,
            (6, Rating::B) => 6.6,
            (6, Rating::A) => 8.0,
            (7, Rating::E) => 8.5,
            (7, Rating::D) => 8.5,
            (7, Rating::C) => 8.5,
            (7, Rating::B) => 10.6,
            (7, Rating::A) => 12.8,
            _ => unreachable!(),
        })
        .into()
    }

    /// Get the fuel consumed by a ship of given mass (including this module) on a jump of given
    /// distance with given FSD module
    ///
    /// # Returns
    /// The amount of fuel consumed by the jump, in tons. If the amount is larger than the maximum
    /// the FSD can consume per jump, the result is wrapped in `Err(_)`, otherwise, it is `Ok(_)`.
    pub fn jump_fuel_consumption(
        &self,
        distance: LightYears,
        ship_mass: Tons,
    ) -> Result<Tons, Tons> {
        let fuel = self.linear_const()
            * (*distance * *ship_mass / *self.optimal_mass()).powf(self.exponent_const());
        if fuel > *self.max_fuel_consumption() {
            Err(fuel.into())
        } else {
            Ok(fuel.into())
        }
    }

    /// Get the distance a ship of given mass (including this module) would jump with given FSD
    /// module
    ///
    /// # Returns
    /// The jump distance, in light years. If the amount of fuel is larger than the maximum
    /// the FSD can consume per jump, the result is wrapped in `Err(_)`, otherwise, it is `Ok(_)`.
    pub fn jump_distance(
        &self,
        fuel_consumed: Tons,
        ship_mass: Tons,
    ) -> Result<LightYears, LightYears> {
        let distance = fuel_consumed.powf(1.0 / self.exponent_const())
            * self.linear_const().powf(1.0 / self.exponent_const())
            * *self.optimal_mass()
            / *ship_mass;
        if *fuel_consumed > *self.max_fuel_consumption() {
            Err(distance.into())
        } else {
            Ok(distance.into())
        }
    }

    /// Computes the actual mass of a ship given its FSD and current range
    pub fn range2current_mass(&self, range: LightYears) -> Tons {
        #[cfg(feature = "optimized-floating-point")] // Optimized formula
        {
            ((*self.optimal_mass() / *range)
                    * (((2.0 * (self.max_fuel_consumption().cbrt() / self.linear_const().cbrt()).ln())
                    / (self.class as u8 as f64 * 0.15 + 1.7))
                    .exp()
                    * (self.max_fuel_consumption().cbrt() / self.linear_const().cbrt())
                    .powf(1.0 / (self.class as u8 as f64 * 0.15 + 1.7)))).into()
            }
        #[cfg(not(feature = "optimized-floating-point"))] // Unoptimized formula
        {
                (self.max_fuel_consumption() / self.linear_const()).powf(1.0 / self.exponent_const())
                    * self.optimal_mass()
                    / range
            }
    }

    /// Computes the fully unladen mass of a ship given its FSD and maximum range
    pub fn range2unladen_mass(&self, range: LightYears) -> Tons {
        (*self.range2current_mass(range) - *self.max_fuel_consumption()).into()
    }

    /// Get the residual mass of a ship, ie the mass difference between fully unladen mass and current
    /// mass, from the maximum range of the ship, and the current real range.
    pub fn residual_mass(&self, max_range: LightYears, current_range: LightYears) -> Tons {
        (*self.range2current_mass(current_range) - *self.range2unladen_mass(max_range)).into()
    }

    // Following methods are used inside this crate to do computations related to FSD jumps
    // Most of the arithmetics below comes from the Hyperspace fuel equation
    // see `hyperspace_fuel_eq.md` at the root of this repo

    /// Get the linear fsd constant of the hyperspace fuel equation
    #[inline(always)]
    pub(crate) fn linear_const(&self) -> f64 {
        match self.rating {
            Rating::A => 0.012,
            Rating::B => 0.01,
            Rating::C => 0.008,
            Rating::D => 0.01,
            Rating::E => 0.011,
            _ => unreachable!(),
        }
    }

    /// Get the inverse of the exponent fsd constant
    #[inline(always)]
    pub(crate) fn exponent_const(&self) -> f64 {
        2.0 + (self.class as u8 - 2) as f64 * 0.15
    }
}

impl fmt::Display for FSD {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}-FSD", self.class, self.rating)
    }
}

impl FromStr for FSD {
    type Err = ParseFSDError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() != 2 {
            return Err(Self::Err::InvalidLength);
        }
        let fsd = FSD::new(
            match s[0..1].parse::<u8>() {
                Ok(class) => match Class::try_from(class) {
                    Ok(class) => class,
                    Err(_) => {
                        return Err(Self::Err::InvalidClass);
                    }
                },
                Err(_) => {
                    return Err(Self::Err::ClassNotANumber);
                }
            },
            match s[1..2].try_into() {
                Ok(rating) => rating,
                Err(_) => {
                    return Err(Self::Err::InvalidRating);
                }
            },
        );

        match fsd {
            Some(fsd) => Ok(fsd),
            None => Err(Self::Err::InvalidPair),
        }
    }
}

/// FSD parsing errors
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum ParseFSDError {
    /// Input isn't 2 characters long
    InvalidLength,
    /// Class character isn't a number
    ClassNotANumber,
    /// No such class exists
    InvalidClass,
    /// No such rating exists
    InvalidRating,
    /// No FSD of such class-Rating pair exists
    InvalidPair,
}

impl fmt::Display for ParseFSDError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ParseFSDError::InvalidLength => write!(f, "should be 2 characters long"),
            ParseFSDError::ClassNotANumber => write!(f, "class should be a number"),
            ParseFSDError::InvalidClass => write!(f, "no such class"),
            ParseFSDError::InvalidRating => write!(f, "no such rating"),
            ParseFSDError::InvalidPair => write!(f, "no fsd with such class-rating pair"),
        }
    }
}

/// A slot that can fit an FSD module
#[derive(Debug)]
pub struct FSDSlot {
    /// The class of the slot, only FSD modules of class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<FSD>,
}

impl FSDSlot {
    /// Get a new FSD slot of given class, fitted with the stock module for that class
    pub fn stock(class: Class) -> Self {
        FSDSlot {
            class,
            module: Some(FSD {
                class,
                rating: Rating::E,
            }),
        }
    }
}
