//! Utility modules

use crate::modules::Class;
use std::fmt;

/// An enum for all utility modules
pub enum UtilityModule {
    /// A heatsink launcher
    Heatsink(Heatsink),
}

impl UtilityModule {
    /// Get the class of the underlying module
    pub fn class(&self) -> Class {
        match self {
            UtilityModule::Heatsink(heatsink) => heatsink.class,
        }
    }
}

impl fmt::Debug for UtilityModule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "U(")?;
        match self {
            UtilityModule::Heatsink(heatsink) => {
                write!(f, "{:?}", heatsink)
            }
        }?;
        write!(f, ")")
    }
}

/// A slot that can fit an utility module
#[derive(Debug)]
pub struct UtilitySlot {
    /// The class of the slot, only utility module of class lesser than or equal to it can fit
    pub class: Class,
    /// The module fitted in the slot
    pub module: Option<UtilityModule>,
}

/// The heatsink module
#[derive(Debug)]
pub struct Heatsink {
    /// The class of the module
    pub class: Class,
}
